# Use an OpenJDK 11 base image
FROM openjdk:11-jre-slim

# Set the working directory inside the container
WORKDIR /app

# Copy project files and directories
COPY plugins/ plugins/
COPY p2/ p2/
COPY tools/ tools/
COPY builtin/ builtin/
COPY configuration/ configuration/
COPY artifacts.xml .
COPY plcverif-web.ini .

# Create files directory
RUN mkdir -p /app/files

RUN chmod -R 777 /app/configuration
RUN chmod -R 777 /app/files
RUN chmod -R 777 /app/tools/nuxmv
RUN chmod -R 777 /app/builtin

# Copy JAR dependencies?
# COPY plugins/*.jar plugins/

#RUN chmod +x /app/tools/nuxmv/nuXmv
#RUN chmod +x /app/tools/nuxmv/NuSMV

#EXPOSE 8082

# Set the entry point to run the executable JAR
CMD ["java", "-jar", "plugins/org.eclipse.equinox.launcher_1.6.0.v20200915-1508.jar", "-configuration", "/app/configuration"]
